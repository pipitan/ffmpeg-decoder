#include <dirent.h>
#include <iostream>
#include <cstdio>
#include "acl/acl.h"
#include "acl/ops/acl_dvpp.h"
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <memory>
extern "C" {
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
}
using namespace std;
const int INPUT_WIDTH = 1280;
const int INPUT_HEIGHT = 720;
//设备类型
int32_t deviceId_ = 0;
aclrtContext context_;
aclrtStream stream_;
pthread_t threadId_;
aclvdecChannelDesc *vdecChannelDesc_;
acldvppStreamDesc *streamInputDesc_;
acldvppPicDesc *picOutputDesc_;
/* 0：H265 main level
 * 1：H264 baseline level
 * 2：H264 main level
 * 3：H264 high level
 */
int32_t enType_ = 1;
// 1：YUV420 semi-planner（nv12）; 2：YVU420 semi-planner（nv21）
int32_t format_ = 1;
void *g_picOutBufferDev;

static bool g_runFlag = true;
void *ThreadFunc(aclrtContext sharedContext)
{
    if (sharedContext == nullptr) {
        ERROR_LOG("sharedContext can not be nullptr");
        return reinterpret_cast<void *>(-1);
    }
    INFO_LOG("use shared context for this thread");
    aclError ret = aclrtSetCurrentContext(sharedContext);
    if (ret != ACL_SUCCESS) {
        ERROR_LOG("aclrtSetCurrentContext failed, errorCode = %d", static_cast<int32_t>(ret));
        return reinterpret_cast<void *>(-1);
    }
    INFO_LOG("process callback thread start ");
    while (g_runFlag) {
        // Notice: timeout 1000ms
        aclError aclRet = aclrtProcessReport(1000);
    }
    return nullptr;
}

void callback(acldvppStreamDesc *input, acldvppPicDesc *output, void *userdata)
{
    /* Get the output memory decoded by VDEC, call the custom function WriteToFile to write
     the data in the output memory to the file, and then call the acldvppFree interface to release
     the output memory */
    cout<<"进入回调函数"<<endl;
    uint32_t inputSize = acldvppGetStreamDescSize(input);
    acldvppStreamFormat inputFormat = acldvppGetStreamDescFormat(input);
    uint64_t inputtamp = acldvppGetStreamDescTimestamp(input);
    uint32_t inputCode = acldvppGetStreamDescRetCode(input);

    printf("inputSize:%d\n", inputSize);
    std::cout<<inputFormat<<std::endl;
    printf("inputtamp:%d\n", inputtamp);
    printf("inputCode:%d\n", inputCode);

    int retCode = acldvppGetPicDescRetCode(output);
    cout<<"retCode:"<<retCode<<endl;
    void *vdecOutBufferDev = acldvppGetPicDescData(output);
    uint32_t size = acldvppGetPicDescSize(output);
    uint32_t height = acldvppGetPicDescHeight(output);
    uint32_t width = acldvppGetPicDescWidth(output);
    std::cout<<"width:"<<width<<std::endl;
    std::cout<<"height:"<<height<<std::endl;
    std::cout<<"size:"<<size<<std::endl;
    cv::Mat yuvImage(cv::Size(1280, 720*3/2), CV_8UC1);
    printf("=================mat======================\n");
//    memcpy(yuvImage.data, vdecOutBufferDev, 1280*720);
    auto aclRet = aclrtMemcpy(yuvImage.data, 1280*720*3/2, vdecOutBufferDev, 1280*720*3/2, ACL_MEMCPY_DEVICE_TO_HOST);
    printf("aclRET:%d", aclRet);
    printf("=================yuvImage======================\n");
    cv::Mat mRGB;
    cv::cvtColor(yuvImage, mRGB, cv::COLOR_YUV2RGB_NV12);
    printf("=================cvtColor======================\n");
    cv::imwrite("1.jpg", mRGB);
    aclError ret = acldvppFree(reinterpret_cast<void *>(vdecOutBufferDev));

    // Release acldvppPicDesc type data, representing output picture description data after decoding
    ret = acldvppDestroyPicDesc(output);

//    count++;
}

bool ReadFileToDeviceMem(const char *fileName, void *&dataDev, uint32_t &dataSize)
{
    // read data from file.
    FILE *fp = fopen(fileName, "rb+");

    fseek(fp, 0, SEEK_END);
    long fileLenLong = ftell(fp);
    fseek(fp, 0, SEEK_SET);

    auto fileLen = static_cast<uint32_t>(fileLenLong);
    void *data = malloc(fileLen);

    size_t readSize = fread(data, 1, fileLen, fp);
    if (readSize < fileLen) {
        free(data);
        fclose(fp);
        return false;
    }

    dataSize = fileLen;
    // Malloc input device memory
    auto aclRet = acldvppMalloc(&dataDev, dataSize);
    // copy input to device memory
    aclRet = aclrtMemcpy(dataDev, dataSize, data, fileLen, ACL_MEMCPY_HOST_TO_DEVICE);
    free(data);
    fclose(fp);
    return true;
}

void FrameDecodeCallback(void* frameData, int frameSize, void *&dct, uint32_t &dctSize)
{
    if ((frameData == nullptr) || (frameSize == 0)) {
        printf("Frame data is null");
    }
    // 将ffmpeg解码得到的h26x数据拷贝到dvpp内存
    cout<<"内存拷贝！"<<endl;
    dctSize = frameSize;
    acldvppMalloc(&dct, dctSize);
    aclError aclRet = aclrtMemcpy(dct, dctSize, frameData, dctSize, ACL_MEMCPY_HOST_TO_DEVICE);
    printf("memery copy:%d", aclRet);
    cout<<"内存拷贝结束！"<<endl;
}

void InitVideoStreamFilter(const AVBitStreamFilter*& videoFilter)
{
    videoFilter = av_bsf_get_by_name("h264_mp4toannexb");
}

int dvppDecode(const char *inVFileName){
    //================================================ffmpeg参数设置===============================================
    // --------------------输入流配置-----------------------
    AVFormatContext *inVFmtCtx = avformat_alloc_context();
    AVStream *inVStream = nullptr;
    int inVStreamIndex = -1;
    AVBSFContext* bsfCtx;
    const AVBitStreamFilter* videoFilter = nullptr;
    InitVideoStreamFilter(videoFilter);
    av_bsf_alloc(videoFilter, &bsfCtx);
    // -------------------打开rtsp协议---------------------
    /*
     *选用tcp协议打开，默认使用的是udp协议，测试udp协议容易产生丢包，导致视频解码不完整
     * 如果使用udp协议打开，一但pkt.flags不为1就会使av_read_frame()失败，返回-541478725（读到文件尾）
     */
    AVDictionary* opts = nullptr;
    av_dict_set(&opts, "rtsp_transport", "tcp", 0);
    if(avformat_open_input(&inVFmtCtx, inVFileName, nullptr, &opts)<0){
        fprintf(stderr,"Cannot open camera.\n");
        return -1;
    }

    // -------------------解封装寻找流信息--------------------
    if(avformat_find_stream_info(inVFmtCtx, nullptr)<0){
        fprintf(stderr,"Cannot find any stream in file.\n");
        return -1;
    }

    //找到rtsp流中最好的视频流索引，一般是分辨率最高的视频流
    inVStreamIndex = av_find_best_stream(inVFmtCtx, AVMEDIA_TYPE_VIDEO, -1, -1, nullptr, 0);
    // 如果没找到视频流的话，stream_index=-1，否则stream_index等于最好视频流索引
    if(inVStreamIndex == -1){
        fprintf(stderr,"Cannot find video stream in file.\n");
        return -1;
    }
    inVStream = inVFmtCtx->streams[inVStreamIndex];
    cout<<"height:"<<inVStream->codecpar->height<<endl;
    cout<<"width:"<<inVStream->codecpar->width<<endl;
    cout<<"codec_id:"<<inVStream->codecpar->codec_id<<endl;
    cout<<"profile:"<<inVStream->codecpar->profile<<endl;

    avcodec_parameters_copy(bsfCtx->par_in, inVStream->codecpar);
    bsfCtx->time_base_in = inVStream->time_base;
    av_bsf_init(bsfCtx);
    // -------------------配置解码器信息---------------------
    //复制一份视频流参数，解码器参数基本对应视频流参数，不需要过多设置
    int g_videoType = inVStream->codecpar->codec_id;
    int g_profile = inVStream->codecpar->profile;
//    cout<<"g_videoType"<<g_videoType<<endl;
//    cout<<"g_profile"<<g_profile<<endl;
    //================================================dvpp参数设置===============================================
    /* 资源管理，创建device, context, stream等*/
    aclError aclRet = aclInit(nullptr);
    aclRet = aclrtSetDevice(deviceId_);
    aclRet = aclrtCreateContext(&context_, deviceId_);
    aclRet = aclrtCreateStream(&stream_);

    /*调用aclrtGetRunMode接口获取软件栈的运行模式,如果调用aclrtGetRunMode接口获取软件栈的运行模式
    为ACL_HOST,则需要通过aclrtMemcpy接口将输入图片数据传输到Device,数据传输完成后,需及时释放内
    存;否则直接申请并使用Device的内存*/
    aclrtRunMode runMode;
    aclrtGetRunMode(&runMode);

    //创建线程tid,并将该tid线程指定为处理Stream上回调函数的线程
    pthread_create(&threadId_, nullptr, ThreadFunc, context_);
    //建视频解码处理的通道描述信息
    vdecChannelDesc_ = aclvdecCreateChannelDesc();
    //设置解码通道属性
    int channelId = 10;
    aclRet = aclvdecSetChannelDescChannelId(vdecChannelDesc_, channelId);
    aclRet = aclvdecSetChannelDescThreadId(vdecChannelDesc_, threadId_);
    /* 回调函数 */
    aclRet = aclvdecSetChannelDescCallback(vdecChannelDesc_, callback);
    // 设置解码器格式
    aclRet = aclvdecSetChannelDescEnType(vdecChannelDesc_, static_cast<acldvppStreamFormat>(enType_));
    // 设置输出图片格式
    aclRet = aclvdecSetChannelDescOutPicFormat(vdecChannelDesc_, static_cast<acldvppPixelFormat>(format_));

    /* 创建视频码流处理的通道 */
    aclRet = aclvdecCreateChannel(vdecChannelDesc_);
    // 创建输入视频流描述信息
    streamInputDesc_ = acldvppCreateStreamDesc();
    //Device存放输入视频数据的内存,inBufferSize_表示内存大小
    void *inBufferDev = nullptr;
    uint32_t inBufferSize = 0;
    size_t dataSize = (inVStream->codecpar->width * inVStream->codecpar->height * 3) / 2;

    // 申请Device内存picOutBufferDev_,用于存放VDEC解码后的输出数据
    aclRet = acldvppMalloc(&g_picOutBufferDev, dataSize);

    // 设置输出图片的描述信息
    picOutputDesc_ = acldvppCreatePicDesc();
    //设置输出图片格式、图片存放位置，大小，像数格式
    aclRet = acldvppSetPicDescData(picOutputDesc_, g_picOutBufferDev);
    aclRet = acldvppSetPicDescSize(picOutputDesc_, dataSize);
    aclRet = acldvppSetPicDescFormat(picOutputDesc_, static_cast<acldvppPixelFormat>(format_));
    //===================================================解码===============================================
    AVPacket pkt;
    int ret;
    while (ret = av_read_frame(inVFmtCtx, &pkt), ret == 0) {
        if (pkt.stream_index != inVStreamIndex) {
            av_packet_unref(&pkt);
            continue;
        }
        if (av_bsf_send_packet(bsfCtx, &pkt)) {
            cout<<"av_bsf_send_packet faild!!"<<endl;
        }
        while ((av_bsf_receive_packet(bsfCtx, &pkt) == 0)) {
            // 将pkt传递给dvpp解码，这里有个问题，传入的是pkt还是pkt.data，大小该如何设置？
            cout<<"pkt.size:"<<pkt.size<<endl;
            cout<<"inBufferSize:"<<inBufferSize<<endl;
            cout<<"typeid(num).name():"<<typeid(pkt.data).name()<< endl;
            aclRet = acldvppMalloc(&inBufferDev, pkt.size);
            aclRet = aclrtMemcpy(inBufferDev, pkt.size, pkt.data, pkt.size, ACL_MEMCPY_HOST_TO_DEVICE);
            inBufferSize = pkt.size;

            cout<<"inBufferSize:"<<inBufferSize<<endl;
            //数据没有进入回掉函数，只是将其复制到了dvpp内存上，数据要和inBufferDev关联上。
            //设置输入视频流描述信息属性值
            cout<<"inBufferDev:"<<inBufferDev<<endl;
            aclRet = acldvppSetStreamDescData(streamInputDesc_, inBufferDev);
            aclRet = acldvppSetStreamDescSize(streamInputDesc_, inBufferSize);
            cout<<"sendFrame begin!!"<<endl;
            aclRet = aclvdecSendFrame(vdecChannelDesc_, streamInputDesc_, picOutputDesc_, nullptr, nullptr);
            cout<<"sendFrame end!!"<<"and aclRet = "<< aclRet<<endl;
        }
        av_packet_unref(&pkt);
        break;
    }
    aclRet = acldvppDestroyStreamDesc(streamInputDesc_);
    aclRet = aclvdecDestroyChannel(vdecChannelDesc_);
    aclvdecDestroyChannelDesc(vdecChannelDesc_);
    vdecChannelDesc_ = nullptr;

    g_runFlag = false;
    void *res = nullptr;
    pthread_join(threadId_, &res);
    acldvppFree(inBufferDev);

    aclrtDestroyStream(stream_);
    stream_ = nullptr;
    aclrtDestroyContext(context_);
    context_ = nullptr;
    aclrtResetDevice(deviceId_);
    aclFinalize();
    return 0;
}
int main(int argc, char *argv[]){
    printf("+++++++++++++进入主函数！++++++++++++++++++++");
    const char *inVFileName = argv[1];
    int ret = dvppDecode(inVFileName);
    std::cout<<ret<<std::endl;
    return 0;
}
